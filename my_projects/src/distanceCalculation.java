import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;
// dla wartości podanych punktów na układzie współrzędnych podaj odległość pomiędzy tymi punktami
public class distanceCalculation {
    public static void main(String[] args) {
        int x1, x2, y1, y2;
        double distance;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Wpisz wartość x1: ");
        Scanner scanner = new Scanner(System.in);
        x1 = scanner.nextInt();
        System.out.println("Wpisz wartość x2: ");
        scanner = new Scanner(System.in);
        x2 = scanner.nextInt();
        System.out.println("Wpisz wartość y1: ");
        scanner = new Scanner(System.in);
        y1 = scanner.nextInt();
        System.out.println("Wpisz wartość y2: ");
        scanner = new Scanner(System.in);
        y2 = scanner.nextInt();
        //System.out.println("x1:" + x1 + "x2:" + x2); sprawdzenie czy dobrze działa wstukiwanie liczb
        distance = Math.sqrt(((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1)));
        System.out.print("Dystans pomiędzy dwoma punktami na układzie współrzędnych to: " + distance);
    }
}
