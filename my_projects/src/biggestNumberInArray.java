import java.util.Arrays;

public class biggestNumberInArray {
    public static void main(String[] args) {
        int arrayLength = 3;
        int[] array = new int[25];

        // wypełnij wartościami

        for (int i = 0; i <array.length; i++){
            array[i] = (int) (Math.random() * 100);
        }
        System.out.println(Arrays.toString(array));

        // znajdź największą liczbę w indeksie
        int biggest = array[0];
        int n;

        for (n = 1; n<array.length; n++){
            if (array[n] > biggest){
                biggest = array[n];
            }

        }

        System.out.println("Biggest number is " + biggest);
    }
}
