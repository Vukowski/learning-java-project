// pierwsze zadanie treningowe; program rysujacy romb przy podanej dlugosci boku.

public class DiamondTask {
    public static void main(String[] args) {
        // typy prymitywne byte, short, int, long - całkowite
        // float, double - wymierne
        // char - znaki
        // boolean - 1/0
        int dlugoscBoku = 8;
        int wiersz = 1;
        for (wiersz = 1; wiersz <= dlugoscBoku; wiersz++) {
            int iloscGwiazdekRosnie = (wiersz * 2) - 1;
            int iloscSpacjiSpada = ((dlugoscBoku * 2) - iloscGwiazdekRosnie) / 2;
            for (int spacje = 1; spacje <= iloscSpacjiSpada; spacje++) {
                System.out.print(" ");
            }
            for (int gwiazdki = 1; gwiazdki <= iloscGwiazdekRosnie; gwiazdki++) {
                System.out.print("*");
            }
            System.out.println(" ");
        }
        int pomocniczy = dlugoscBoku;
        for (int nowyWiersz = 1; nowyWiersz <= (dlugoscBoku - 1); nowyWiersz++) {
            int iloscGwiazdekSpada = (pomocniczy*2) -3;
            for (int spacje = 1; spacje <= nowyWiersz; spacje++) {
                System.out.print(" ");
            }
            for (int gwiazdki = 1; gwiazdki <= iloscGwiazdekSpada; gwiazdki++) {
                System.out.print("*");
            }
            System.out.println(" ");
            pomocniczy--;
        }
    }
}
