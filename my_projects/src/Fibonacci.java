// program podający wartość dla podanego numeru w ciągu fibonacciego
public class Fibonacci {
    public static void main(String[] args) {
        Fibonacci fibonacci = new Fibonacci();
        System.out.println(fibonacci.fib(5));
    }
    public int fib(int N) {
        if (N == 0)
            return 0; // czy podałeś 0
        else if (N == 1)
            return 1;   // czy podałeś 1
        else
            return (fib(N - 1) + fib(N - 2)); // wywołuj siebie tak długo, aż przejdziesz całym ciągiem do wyniku i zsumujesz ciąg
    }
}

