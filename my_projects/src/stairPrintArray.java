import java.util.Arrays;

public class stairPrintArray {
    public static void main(String[] args) {
        int[][] array={{1},{2,3},{4,5,6},{7,8,9,0}};
        System.out.println(Arrays.deepToString(array));
        System.out.println(Arrays.toString(array[2]));
        for(int i=0; i<array.length; i++){
            System.out.println(Arrays.toString(array[i]));
        }
        int[][][] bigArray={{{1},{2,3},{4,5,6},{7,8,9,0}},{{1},{2,3},{4,5,6},{7,8,9,0}}};
        System.out.println(Arrays.deepToString(bigArray));

        //for each loop
        for(int[] littleArray : array){
            for(int element : littleArray){
                System.out.print(element);
            }
            System.out.println();
        }
        System.out.println(bigArray.length );



    }
}
