import java.util.Arrays;

public class arrayLearning {
    public static void main(String[] args) {
        // deklaracja i inicjalizacja pustej tablicy

        int [] tablicaLiczb1 = new int[10];
        int tablicaLiczb2 []; // notacja nie polecana

        // uzupełnianie wartościami

        tablicaLiczb1[0] = 10;
        tablicaLiczb1[1] = 13;

        //itd.

        // deklaracja i inicjalizacja tablicy z wartościami notacja WĄSY

        int [] tablicaLiczb3 = {10,13};

        int arrayLength = 15;
        int [] testArray = new int[arrayLength];

        for (int i =0; i< arrayLength; i++){
            testArray[i] = (i);
            System.out.println(testArray[i]);
        }
        testArray[4] = 29;
        System.out.println(Arrays.toString(testArray));
        System.out.println(testArray.length);
        System.out.println(Arrays.toString(tablicaLiczb3));

        // fibonacci w formie tablicy!

        int n = 20;
        int[] fibonacciArray = new int[n];
        for (int i = 0; i < fibonacciArray.length; i++){
            if (i==0){
                fibonacciArray[i] = 0;
            } else
                if (i==1){
                    fibonacciArray[i] = 1;
                } else
                    fibonacciArray[i] = fibonacciArray[(i-2)]+fibonacciArray[(i-1)];
        }
        System.out.println(Arrays.toString(fibonacciArray));




    }
}
