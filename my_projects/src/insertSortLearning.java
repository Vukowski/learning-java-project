import java.util.Arrays;

public class insertSortLearning {
    public static void main(String[] args) {
        int[] unsortedArray = {29, 13, 509, 1, 0, 26, 32, 57, 14, 200, 38, 77};
        //System.out.println(Arrays.toString(unsortedArray));
        //int[] insertionSortedArray = insertionSort(unsortedArray);
        //System.out.println(Arrays.toString(insertionSortedArray));
        System.out.println(Arrays.toString(unsortedArray));
        int[] bubbleSortedArray = bubbleSort(unsortedArray);
        System.out.println(Arrays.toString(bubbleSortedArray));
    }
    public static int[] bubbleSort (int[] arrayBubble){
        int n, temp;
        n = arrayBubble.length;
        for (int i = 0; i < n-1; i++){                      // tyle obrotów, ile jest liczb w tablicy
            for (int j = 0; j < n-i-1; j++){                // tyle powtórzeń, ile jest liczb w tablicy - ilość wykonanych obrotów
               if (arrayBubble[j]>arrayBubble[j+1]){
                   temp = arrayBubble[j+1];
                   arrayBubble[j+1]=arrayBubble[j];
                   arrayBubble[j]=temp;
               }

            }
        }
        return arrayBubble;
    }
    public static int[] insertionSort (int[] arrayInsert){
        int n, m, key;
        n = arrayInsert.length;
        for (int i = 1; i < n; i++){
            key = arrayInsert[i];                           // zmienna porównywana
            m =  i - 1;                                     // licznik do ilości sprawdzeń w ramach 'obrotu' jednej zmiennej
            while ((m > -1) && (arrayInsert[m]>key)){       // 'przeskocz' porównywaną zmienną o oczko wyżej, gdy spełnione i szukaj dalej
                arrayInsert[m+1]=arrayInsert[m];
                m--;
            }
            arrayInsert[m+1]=key;                            // znalazłem dom dla sortowanej zmiennej
        }
        return arrayInsert;
    }
}

