import java.sql.SQLOutput;
import java.util.Arrays;
import java.util.Scanner;

public class ticTacToe {
    public static void main(String[] args) {
        String[][] board = new String[4][4];
        board[0][0] = "*  ";
        board[0][1] = "A1 ";
        board[0][2] = "A2 ";
        board[0][3] = "A3 ";
        board[1][0] = "B1 ";
        board[2][0] = "B2 ";
        board[3][0] = "B3 ";
        for(int m =0; m<board.length-1; m++){
            for(int n=0; n<board.length-1; n++){
                board[m+1][n+1] = " ";
            }
        }

        for(int i = 0; i<board.length; i++){
            for(int j = 0; j<board.length;j++){
                System.out.print(board[i][j]);
            }
            System.out.println();
        }

        int counter = 0;
        int playerCounter = 0;
        do{
            if(playerCounter==0){
                System.out.println("Ruch pierwszego gracza - kolumna, wiersz: ");
                Scanner playerOneColumn = new Scanner(System.in);
                int column = playerOneColumn.nextInt();
                Scanner playerOneRow = new Scanner(System.in);
                int row = playerOneRow.nextInt();
                board[row][column] = "X ";
                playerCounter=1;
                counter++;
                for(int i = 0; i<board.length; i++){
                    for(int j = 0; j<board.length;j++){
                        System.out.print(board[i][j]);
                    }
                    System.out.println();
                }
            } else if (playerCounter==1){
                System.out.println("Ruch drugiego gracza - kolumna, wiersz: ");
                Scanner playerTwoColumn = new Scanner(System.in);
                int column = playerTwoColumn.nextInt();
                Scanner playerTwoRow = new Scanner(System.in);
                int row = playerTwoRow.nextInt();
                board[row][column] = "O ";
                playerCounter=0;
                counter++;
                for(int i = 0; i<board.length; i++){
                    for(int j = 0; j<board.length;j++){
                        System.out.print(board[i][j]);
                    }
                    System.out.println();
                }
            }else if(counter==9){
                System.out.println("GAME OVER");
            }

        } while (counter!=9);


    }
}
// todo exception occupied slot, end game if a player achieves a continous line of 3 characters
//
