import java.util.Scanner;
// nauka operowania if, if else, else oraz operatorami logicznymi
/* kalkulator liczący: przy podanych dwóch liczbach oraz numerze identyfikacyjnym operacji, przeprowadź odpowiednie obliczenie
mając na uwadze, żeby uniknąć dzielenia przez zero lub wyboru nieznanej operacji
 */
public class calculatorIfTutorial {
    public static void main(String[] args) {
        double firstNumber, secondNumber;
        System.out.print("Podaj wartość liczby pierwszej: ");
        Scanner scannerFirst = new Scanner(System.in);
        firstNumber = scannerFirst.nextDouble();
        System.out.print('\n' + "Podaj wartość liczby drugiej: ");
        Scanner scannerSecond = new Scanner(System.in);
        secondNumber = scannerSecond.nextDouble();
        int operation;
        System.out.print('\n' + "Podaj numer operacji, gdzie 1 = +, 2 = -, 3 = *, 4 = /: ");
        Scanner scannerOperation = new Scanner(System.in);
        operation = scannerOperation.nextInt();
        if (operation == 1) {
            System.out.println((firstNumber + secondNumber));
        }
            else if (operation == 2){
                System.out.println((firstNumber - secondNumber));
            }
            else if (operation == 3) {
            System.out.println((firstNumber * secondNumber));
        }
            else if (operation == 4 && secondNumber != 0){
            System.out.println((firstNumber / secondNumber));
        }
            else if (operation == 4) {
            System.out.println("Próba dzielenia przez zero!");
        }
            else {
            System.out.println("Nieprawidłowy numer operacji!");
        }
        }
        }

